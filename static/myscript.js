$(document).ready(function(){	

    $("#myform").submit(function(){
 
          var search = $("#books").val();
          if(search == "")
          {
              alert("Field can't be empty!");
          }
          else{		
          var title = "";
 
          $.get("https://www.googleapis.com/books/v1/volumes?q=" + search,function(response){
            
          console.log(response);
          for(i=0;i<response.items.length;i++)
          {
           title=$('<tr><th>' + (i + 1) + "</th>"+
           '<td class="align-middle">' + response.items[i].volumeInfo.title +'</td>'+
           '<td class="align-middle">' + response.items[i].volumeInfo.authors + '</td>'+
           "<td> <img class='img-fluid' style='width:22vh' src='" +response.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>"+
           '<br><a href=' + response.items[i].volumeInfo.infoLink + '><button class="btn btn-outline-light">Read More</button></a>'+'</td>'+
           '<td class="align-middle">' + response.items[i].volumeInfo.publisher + '</td>'+
           '<td class="align-middle">' + response.items[i].volumeInfo.publishedDate + '</td>'+
           "<td class='align-middle'>" +
            "<button  class='btn btn-outline-light' type ='button' onclick='liked("+i+")'> Like</button>"+
            "<button  class='btn btn-outline-light' type ='button' onclick='unliked("+i+")'> Dislike</button>"+
            "<p id ='like"+ i +"' value='0'>0</p>"+'</td></tr>');  

           title.appendTo('#content');
          }
   	  });
       
       }
       return false;
    });
 
 });

 function liked(i){
    var data = JSON.stringify(response.items[i]);
    var xhttpr = new XMLHttpRequest();
    xhttpr.onreadystatechange= function(){
        if(this.readyState==4 && this.status ==200){
            var like = JSON.parse(this.responseText).like;
            $("#like"+i).html(like);
        }
    }
        xhttpr.open("POST","/api/likedbooks/");
        xhttpr.send(data);

}

function unliked(i){
    var data = JSON.stringify(response.items[i]);
    var xhttpr = new XMLHttpRequest();
    xhttpr.onreadystatechange= function(){
        if(this.readyState==4 && this.status ==200){
            var unliked = JSON.parse(this.responseText).unlike;
            $("#like"+i).html(unliked);
        }
    }
        xhttpr.open("POST","/api/unlikedbooks/");
        xhttpr.send(data);

}

function topbooks(){
    var xhttpr = new XMLHttpRequest();
    xhttpr.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            var top = JSON.parse(this.responseText).top_list;
            $('.modal-body').html('');
            for (var i=0;i<5;i++){
                $('.modal-body').append(
                    "<p id ='fav'>"+top[i]['title']+"</p>"
                )
            }
        }
    }
    xhttpr.open("GET","/topbook/",true)
    xhttpr.send();
}