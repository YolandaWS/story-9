from django.db import models

class Book(models.Model):
    b_id = models.CharField(max_length=10,primary_key=True)
    title = models.CharField(max_length=200)
    authors = models.CharField(max_length=200,null=True)
    image = models.TextField()
    publisher = models.CharField(max_length=200, null=True)
    date = models.CharField(max_length=200,null=True)
    like = models.IntegerField(default=0)

    def __str__(self):
        return self.title
