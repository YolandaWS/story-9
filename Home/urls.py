from django.urls import path
from .views import index,like,unlike,topbooks

urlpatterns = [
    path('', index, name='home'),
    path('api/likedbooks/', like),
    path('api/unlikedbooks/',unlike),
    path('topbooks/',topbooks)
]