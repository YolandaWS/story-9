from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import index

# Create your tests here.
class Story9UnitTest(TestCase):

    def test_page(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)
    
    def test_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response,'Home/index.html')
    
    def test_header(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Story9-Yolandaws",html_response)

class Story9FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('chromedriver', chrome_options=chrome_options)
		super(Story9FunctionalTest, self).setUp()
    
	def tearDown(self):
		self.selenium.quit()
		super(Story9FunctionalTest, self).tearDown()
    
	def test_header_page(self):
		browser=self.selenium
		self.assertIn('',browser.title)
