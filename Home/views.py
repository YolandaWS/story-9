import json
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from .models import Book
from django.views.decorators.csrf import csrf_exempt

def index(request):
    return render(request,'Home/index.html')

@csrf_exempt
def like(request):
    data = json.loads(request.body)
    try:
        book_d = books.objects.get(b_id= data['id'])
        book_d.like +=1
        book_d.save()
    except:
        book_d = Book.objects.create(
            b_id = data['id'],
            image = data['volumeInfo']['imageLinks']['smallThumbnail'],
            title = data['volumeInfo']['title'],
            authors = data['volumeInfo']['authors'],
            publisher = data['volumeInfo']['publisher'],
            publisher_date = data['volumeInfo']['publishedDate'],
            like = 1
            )
    return JsonResponse({'like': book_d.like})

@csrf_exempt
def unlike(request):
    data = json.loads(request.body)
    print(data)
    try:
        book_d = Book.objects.get(b_id= data['id'])
        if book_d.like>0:
            book_d.like -=1
        book_d.save()
    except:
        book_d = Book.objects.create(
            b_id = data['id'],
            image = data['volumeInfo']['imageLinks']['smallThumbnail'],
            title = data['volumeInfo']['title'],
            authors = data['volumeInfo']['authors'],
            publisher = data['volumeInfo']['publisher'],
            publisher_date = data['volumeInfo']['publishedDate'],
            )
    return JsonResponse({'unlike': book_d.like})

def topbooks(request):
    top_book= Book.objects.order_by('-like')
    top_list =[]
    for x in top_book:
       top_list.append({'image':x.image,'title': x.title,'authors':x.authors,'publisher':x.publisher,'published_date':x.publisher_date,'like':x.like})
    return JsonResponse({'top_list':top_list})